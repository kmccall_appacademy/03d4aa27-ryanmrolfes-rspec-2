def reverser(&prc)
  sentence = yield
  words = sentence.split
  words.map { |word| word.reverse }.join(' ')
end

def adder(num = 1, &prc)
  yield + num
end

def repeater(num = 1, &prc)
  num.times do
    yield
  end
end
